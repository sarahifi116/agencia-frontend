//templateUrl start from index.html to the template
angular
  .module('marcas')
  .component('marcas', {
    templateUrl: 'app/marcas/marcas.template.html',
    controller: ['MarcaService','$window',
        function MarcaController(MarcaService,$window) {
          
          var marcasController = this; 
          var idMarca = 0;

          marcasController.marcas = MarcaService.query();

          marcasController.abrirModal = function(marca){
            document.getElementById('ModalTitulo').innerHTML = 'Editar Marca';
            document.getElementById('inputMarca').value = marca.nombre;
            idMarca = marca.id;
          };

          marcasController.guardar = function(){
            var nombreMarca = document.getElementById('inputMarca').value;
            var tipoGuardado = document.getElementById('ModalTitulo').innerHTML;

            if(tipoGuardado === 'Editar Marca'){
              MarcaService.update({id:idMarca},{nombre:nombreMarca}).$promise.then((result) => {
                if (result) {
                  $('#modal').modal('hide');
                  $window.location.reload();
                }
              });
            }else{
              MarcaService.save({nombre:nombreMarca}).$promise.then((result) => {
                if (result) {
                  $('#modal').modal('hide');
                  $window.location.reload();
                }
              });
            }
            document.getElementById('ModalTitulo').innerHTML = 'Nueva Marca';
          };

          marcasController.eliminar = function(marca){
            MarcaService.delete({id:marca.id}).$promise.then((result) => {
              if (result) {
                $window.location.reload();
              }
            });
          };

          $(".letras-only").bind('keypress', function(event) {
            var regex = new RegExp("^[a-zA-Z ]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);
            if (!regex.test(key)) {
              event.preventDefault();
              return false;
            }
          });
        }
    ],
    controllerAs: 'marcasController',
  });