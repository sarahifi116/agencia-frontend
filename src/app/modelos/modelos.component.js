angular
  .module('modelos')
  .component('modelos', {
    templateUrl: 'app/modelos/modelos.template.html',
    controller: ['ModelosService','MarcaService','TiposService','$window',
        function ModelosController(ModelosService,MarcaService,TiposService,$window) {

          var modelosController = this;
          var idModelo = 0;

          modelosController.marcas = MarcaService.query();
          modelosController.tipos = TiposService.query();
          modelosController.modelos = ModelosService.query();  

          modelosController.abrirModal = function(modelo){
            document.getElementById('ModalTitulo').innerHTML = 'Editar Modelo';
            document.getElementById('inputModelo').value = modelo.nombre;
            modelosController.marca = modelo.idMarca;
            modelosController.tipo = modelo.idTipo;
            idModelo = modelo.id;
          };

          modelosController.guardar = function(){
            var nombreModelo = document.getElementById('inputModelo').value;
            var idMarca = modelosController.marca;
            var idTipo = modelosController.tipo;

            var tipoGuardado = document.getElementById('ModalTitulo').innerHTML;

            if(tipoGuardado === 'Editar Modelo'){
              ModelosService.update({id:idModelo},{nombre:nombreModelo,idMarca:idMarca,idTipo:idTipo}).$promise.then((result) => {
                if (result) {
                  $('#modal').modal('hide');
                  $window.location.reload();
                }
              });
            }else{
              ModelosService.save({nombre:nombreModelo,idMarca:idMarca,idTipo:idTipo}).$promise.then((result) => {
                if (result) {
                  $('#modal').modal('hide');
                  $window.location.reload();
                }
              });
            }
            document.getElementById('ModalTitulo').innerHTML = 'Nuevo Modelo';
          };

          modelosController.eliminar = function(modelo){
            ModelosService.delete({id:modelo.id}).$promise.then((result) => {
              if (result) {
                $window.location.reload();
              }
            });
          };

          modelosController.cancelar = function(){
            document.getElementById('ModalTitulo').innerHTML = 'Nuevo Modelo';
            document.getElementById('inputModelo').value = '';
            modelosController.marca = undefined;
            modelosController.tipo = undefined;
          };
        }
    ],
    controllerAs: 'modelosController',
  });