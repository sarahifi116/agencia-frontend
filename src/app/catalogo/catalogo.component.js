angular
  .module('catalogo')
  .component('catalogo', {
    templateUrl: 'app/catalogo/catalogo.template.html',
    controller: ['InventarioService',
        function CatalogoController(InventarioService) {
          var catalogoController = this;

          catalogoController.inventario = InventarioService.query();
        }
    ],
    controllerAs: 'catalogoController',
  });