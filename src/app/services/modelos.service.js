angular
  .module('services')
  .factory('ModelosService', ['$resource',
    ($resource) => {
      return $resource(`${API_URL}/modelos/:id`, {}, {
        update: {
          method: 'PUT',
        },
      });
    }
  ]);