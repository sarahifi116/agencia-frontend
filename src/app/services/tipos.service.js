angular
  .module('services')
  .factory('TiposService', ['$resource',
    ($resource) => {
      return $resource(`${API_URL}/tipos/:id`, {}, {});
    }
  ]);