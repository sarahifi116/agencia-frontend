angular
  .module('services')
  .factory('ColorService', ['$resource',
    ($resource) => {
      return $resource(`${API_URL}/colores/:id`, {}, {});
    }
  ]);
