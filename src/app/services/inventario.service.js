angular
  .module('services')
  .factory('InventarioService', ['$resource',
    ($resource) => {
      return $resource(`${API_URL}/inventario/:id`, {id: '@id'}, {
        update: {
            method: 'PUT',
        },
      });
    }
  ]);