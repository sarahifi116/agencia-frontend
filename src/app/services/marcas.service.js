angular
  .module('services')
  .factory('MarcaService', ['$resource',
    ($resource) => {
      return $resource(`${API_URL}/marcas/:id`, {}, {
        update: {
            method: 'PUT',
        },
      });
    }
  ]);
