angular
  .module('inventario')
  .component('inventario', {
    templateUrl: 'app/inventario/inventario.template.html',
    controller: ['InventarioService','ModelosService','MarcaService','TiposService','ColorService','$window',
        function InventarioController(InventarioService,ModelosService,MarcaService,TiposService,ColorService,$window) {
          var inventarioController = this;
          inventarioController.idInventario = 0;
          var guardar = 1;

          inventarioController.marcas = MarcaService.query();
          inventarioController.tipos = TiposService.query();
          inventarioController.modelos = ModelosService.query();
          inventarioController.colores = ColorService.query();
          inventarioController.inventario = InventarioService.query();

          inventarioController.modeloMarcaTipo = function(){
            var idModelo = inventarioController.modelo;
            var length = inventarioController.modelos.length;

            for(i = 0; i < length; i++){
              if(idModelo == inventarioController.modelos[i].id){
                inventarioController.marca = inventarioController.modelos[i].idMarca;
                inventarioController.tipo = inventarioController.modelos[i].idTipo;
              }
            }
          };

          inventarioController.guardar = function(){
            var idModelo = 0;
            var idColor = 0;
            var fechaIngreso = document.getElementById('fechaIngreso').value;
            var kilometraje = document.getElementById('inputKilometraje').value;

            if(inventarioController.modelo == undefined || inventarioController.modelo == ''){
              document.getElementById('mensajeModal').innerHTML = 'favor de seleccionar un modelo';
            }else if(inventarioController.color == undefined || inventarioController.color == ''){
              document.getElementById('mensajeModal').innerHTML = 'favor de seleccionar un color';
            }else if(fechaIngreso == undefined || fechaIngreso == ''){
              document.getElementById('mensajeModal').innerHTML = 'favor de seleccionar una fecha de ingreso';
            }else if(kilometraje == undefined || kilometraje == ''){
              document.getElementById('mensajeModal').innerHTML = 'favor de capturar kilometraje';
            }else{
              idModelo = inventarioController.modelo;
              idColor = inventarioController.color;            

              if(guardar == 0){
                InventarioService.update({id:inventarioController.idInventario},{idModelo:idModelo,idColor:idColor,kilometraje:kilometraje,fechaIngreso:fechaIngreso}).$promise.then((result) => {
                  if (result) {
                  document.getElementById('mensajeModal').innerHTML = 'se actualizo correctamente el inventario';
                }else{
                  document.getElementById('mensajeModal').innerHTML = 'algo salio mal al intentar actualizar';
                }
              });
              }else{
                InventarioService.save({idModelo:idModelo,idColor:idColor,kilometraje:kilometraje,fechaIngreso:fechaIngreso}).$promise.then((result) => {
                  if (result) {
                  document.getElementById('mensajeModal').innerHTML = 'se agrego correctamente al inventario';
                  }
                  else{
                    document.getElementById('mensajeModal').innerHTML = 'algo salio mal al intentar agregar';
                  }
                });
              }
            }
          };

          inventarioController.reload = function(){
            var mensaje = document.getElementById('mensajeModal').innerHTML;
            if(mensaje == 'se agrego correctamente al inventario' || mensaje == 'se actualizo correctamente el inventario'){
              $window.location.reload();
            }
          }

          inventarioController.eliminar = function(inv){
            InventarioService.delete({id:inv.id}).$promise.then((result) => {
              if (result) {
                 $window.location.reload();
              }
            });
          };

          inventarioController.editar = function(inv){
            guardar = 0;
            inventarioController.idInventario = inv.id;
            inventarioController.modelo = inv.idModelo;
            inventarioController.marca = inv.idMarca;
            inventarioController.tipo = inv.idTipo;
            inventarioController.color = inv.idColor;
            document.getElementById('inputKilometraje').value = inv.kilometraje;
            document.getElementById('fechaIngreso').value = inv.fechaIngreso;
          };

          $(function(){

            $('.number-only').keypress(function(e) {
                  if(isNaN(this.value+""+String.fromCharCode(e.charCode))) return false;
            })
            .on("cut copy paste",function(e){
                  e.preventDefault();
            });
          
          });
        }
    ],
    controllerAs: 'inventarioController',
  });