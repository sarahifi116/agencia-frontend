angular.module('agenciaApp', [
        'marcas',
        'catalogo',
        'inventario',
        'modelos',
        'ngRoute',
    ])
    .config(['$qProvider', function ($qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
    }]);


const API_URL = 'https://app-e632cdc3-1a69-4c90-8d45-f4ea35aa14a2.cleverapps.io';
