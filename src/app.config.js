angular.
  module('agenciaApp').
  config(['$routeProvider',
  function config($routeProvider) {
    $routeProvider
    .when('/home', {
        template: '<catalogo></catalogo>'
      })
      .when('/marca', {
        template: '<marcas></marcas>'
      })
      .when('/modelo', {
        template: '<modelos></modelos>'
      })
      .when('/inventario', {
        template: '<inventario></inventario>'
      })
      .otherwise('/home');
  }
  ]);
