const cors = require('cors');
const express = require('express');
const app = express();

const dotenv = require('dotenv');
dotenv.config();

const port = process.env.PORT;

app.use(express.static(__dirname + '/src'));
app.use(cors());

app.listen(port||3000, () => {
    console.log(`Running in: http://localhost:${port}`);
});